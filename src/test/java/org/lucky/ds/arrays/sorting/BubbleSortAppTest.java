package org.lucky.ds.arrays.sorting;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.when;
class BubbleSortAppTest {

    @Mock
    BubbleSort bubbleSort;

    @InjectMocks
    BubbleSortApp bubbleSortApp;

    @BeforeEach
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void callBubbleSort() {
        int []inputArray={5, 6, 4, 7, 9, 2, 1, 0, 15, 17, 11};
        int[] outArray={0,1	,2,	4,	5,	6,	7,	9,	11,	15,	17};
        when(bubbleSort.sorttedArray(inputArray))
                       .thenReturn(outArray);
        bubbleSortApp.callBubbleSort(inputArray);
    }
}