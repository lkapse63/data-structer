package org.lucky.ds.arrays.sorting;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class BubbleSortTest {

    @Test
    void sorttedArray() {
        int []inputArray={5, 6, 4, 7, 9, 2, 1, 0, 15, 17, 11};
        int[] outArray={0,1	,2,	4,	5,	6,	7,	9,	11,	15,	17};
         BubbleSort bubbleSort =new BubbleSort();
         int[] sorttedArray = bubbleSort.sorttedArray(inputArray);
         assertArrayEquals(sorttedArray,outArray,"Outout array missmached");
    }
}