package org.lucky.ds.functions;

import org.junit.jupiter.api.Test;
import static org.mockito.Mock.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;

class FunctionsDemoTest {

    @Test
    void modified() {
        FunctionsDemo functionsDemo = mock(FunctionsDemo.class);
        doNothing()
                .when(functionsDemo)
                .modified(10,20);

        functionsDemo.modified(11,20);
    }
}