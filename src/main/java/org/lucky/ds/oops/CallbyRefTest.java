package org.lucky.ds.oops;

public class CallbyRefTest {

    public int add(int num1, int num2){
        return num1+=num2;
    }

    public static void main(String[] args) {
        CallbyRefTest obj = new CallbyRefTest();
        int a=10;
        int b=20;
        System.out.println(obj.add(a,b));
        System.out.println(a+" "+b);
    }
}
