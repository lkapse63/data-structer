package org.lucky.ds.oops;

import java.io.FileReader;
import java.io.IOException;

public class FunctionCall {

    public static void main(String[] args)throws Exception {

        FunctionCall obj=new FunctionCall();
        obj.callAdd();
        obj.fileReader();
    }


    public int add(int a, int b){
        b=a+b;
        return b;
    }

    public void callAdd(){
        int a=10;
        int b=20;
        a=add(a,b);
        System.out.println("a:"+a);
        System.out.println("b:"+b);
    }


    public void close(FileReader reader){
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void fileReader() throws Exception{
        FileReader reader =new FileReader("D:\\Projects\\BAXA_PROJECT\\GIT-SI\\BAXA_SIService\\catalina.home_IS_UNDEFINED\\logs\\SI_Scheduler\\SIScheduler.log");
        close(reader);
        System.out.println(reader.read());
//        reader.close();
    }

}
