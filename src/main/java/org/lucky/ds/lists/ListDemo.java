package org.lucky.ds.lists;



public class ListDemo {

	public static void main(String[] args) {
		ListImpl<Integer> demo=new ListImpl();
		demo.add(10);
		demo.add(11);
		demo.add(12);
		demo.add(13);
		
		demo.addAfter(14, 13);
		
//		demo.deleteAfter(11);
		demo.print();

		demo.clear();
		demo.print();

	}

}
