package org.lucky.ds.lists;

public class Node<T> implements Comparable<T> {

	private T value;
	private Node<T> nextRef;
	/**
	 * @return the value
	 */
	public T getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(T value) {
		this.value = value;
	}
	/**
	 * @return the nextRef
	 */
	public Node<T> getNextRef() {
		return nextRef;
	}
	/**
	 * @param nextRef the nextRef to set
	 */
	public void setNextRef(Node<T> nextRef) {
		this.nextRef = nextRef;
	}
	@Override
	public int compareTo(T o) {
		if(o==this.value)
			return 0;
		return 1;
	}
	
	
}
