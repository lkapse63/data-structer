package org.lucky.ds.lists;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.lucky.ds.OmniFileCopy;

public class ListImpl<T> {

	private Node<T> head;
	private Node<T> tail;

	public static Logger logger = Logger.getLogger(ListImpl.class);
	public void add(T element){
		Node<T> newNode=new Node<>();
		newNode.setValue(element);
		
		if(head==null){
			head=newNode;
			tail=newNode;
		}else{
			tail.setNextRef(newNode);
			tail=newNode;
		}
	}
	
	 public void addAfter(T element, T after){
		 
		 Node<T> tmp=head;
		 Node<T> refNode=null;
		 
		 while(true){
			 if(tmp==null)
				 break;
			 if(tmp.compareTo(after)==0){
				 refNode=tmp;
				 break;
			 }
			 tmp=tmp.getNextRef();
		 }
		 
		 if(refNode !=null){
			 Node<T> nd=new Node<>();
			 nd.setValue(element);
			 nd.setNextRef(tmp.getNextRef());
			 if(tmp == tail){
				 tail=nd;
			 }
			 tmp.setNextRef(nd);
		 }else
			 logger.info("Unable to find element");
		 
	 }
	 
	 public void deleteAfter(T after){
         
	        Node<T> tmp = head;
	        Node<T> refNode = null;
	        logger.info("Traversing to all nodes..");
	        /**
	         * Traverse till given element
	         */
	        while(true){
	            if(tmp == null){
	                break;
	            }
	            if(tmp.compareTo(after) == 0){
	                //found the target node, add after this node
	                refNode = tmp;
	                break;
	            }
	            tmp = tmp.getNextRef();
	        }
	        if(refNode != null){
	            tmp = refNode.getNextRef();
	            refNode.setNextRef(tmp.getNextRef());
	            if(refNode.getNextRef() == null){
	                tail = refNode;
	            }
	            logger.info("Deleted: "+tmp.getValue());
	        } else {
	        	logger.info("Unable to find the given element...");
	        }
	    }
	 
	 
	 public void clear(){
		 head=null;
	 }
	 
	 
	 public int size(){
		 int counter=1;
		 if(head==null)
			 return 0;
		 Node<T> tr=head;
		 while(true){
			 if(tr.getNextRef()==null)
				 break;
			 counter++;
			 tr=tr.getNextRef();
		 }
		 return counter;
	 }
	 
	 
	 public void remove(T elemment){
		 if(head==null)
			 logger.info("list is empty");
	
		 if(head.compareTo(elemment)==0){
			 head=null;
			 return;
		 }
		 Node<T> curr=head;
		 Node<T> prev=null;
		 while(curr.getNextRef()!=null){
			 
			 if(curr.compareTo(elemment)==0){
				 
			 }
		 }
	 }
	 
	public void print(){
        
        Node<T> tmp = head;
        while(true){
            if(tmp == null){
                break;
            }
            logger.info(""+tmp.getValue());
            tmp = tmp.getNextRef();
        }
    }
}
