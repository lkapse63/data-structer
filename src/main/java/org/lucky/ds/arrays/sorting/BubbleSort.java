package org.lucky.ds.arrays.sorting;

public class BubbleSort {

	public static void main(String[] args) {
		int []inputArray={5, 6, 4, 7, 9, 2, 1, 0, 15, 17, 11};
		BubbleSort bubbleSort =new BubbleSort();
		int[] sorttedArray = bubbleSort.sorttedArray(inputArray);
		// System.out.println(sorttedArray);


	}

	public int[] sorttedArray(int []inputArray){
		System.out.println("input array");
		for(int a : inputArray)
			System.out.print("\t"+a);
		int n= inputArray.length;
		int loopcount=0;
		for (int i=0; i<n;i++){
			boolean swap=false;
			for (int j=0;j < n-i-1;j++){
				loopcount++;
				if(inputArray[j]> inputArray[j+1]){
					int tmp= inputArray[j];
					inputArray[j]=inputArray[j+1];
					inputArray[j+1]=tmp;
					swap=true;
				}

			}
			if(!swap)
				break;
		}

		System.out.println("\n sorted array");
		for(int a : inputArray)
			System.out.print("\t"+a);

		System.out.println("\nloopcount:\n"+loopcount);
		return inputArray;
	}

}
