package org.lucky.ds.arrays.sorting;

public class Testing {

	public static void main(String[] args) {
		int arr[]= {3,5,1};
		
		for(int i=0; i < arr.length; i++){
			
			for(int j=0; j< arr.length-i-1; j++){
				if(arr[j] > arr[j+1]){
					int tmp=arr[j];
					arr[j]=arr[j+1];
					arr[j+1]=tmp;
				}
			}
		}
		
		print(arr);

	}
	
	public static void print(int [] arr){
		System.out.println();
		for(int a : arr)
			System.out.print(a+"\t");
		System.out.println();
	}

}
