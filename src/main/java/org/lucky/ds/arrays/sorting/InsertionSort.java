package org.lucky.ds.arrays.sorting;

public class InsertionSort {

	public static void main(String[] args) {
		int arr[]= {3,5,1,2,4,9,8,7,6};
		System.out.println("Input array");
		print(arr);
		int loopcount=0;
		for (int i=1; i< arr.length; i++){
			int tmp=arr[i];
			int j=i-1;	
			while(j>=0 && tmp < arr[j]){
				arr[j+1]= arr[j];
				j=j-1;	
				loopcount++;
			}
			arr[j+1]=tmp;
			
		}
		
		System.out.println("Total loop count: "+loopcount);
		System.out.println("Sorted array");
		print(arr);
	}
	
	public static void print(int [] arr){
		StringBuffer buff = new StringBuffer();
		buff.append("[");
		for(int a : arr)
			buff.append(a+",");
		String trimmed = replaceLast(buff.toString().trim(),",","");
		System.out.println(trimmed+"]");
	}

	public static String replaceLast(String str,String ori, String another){
		return new StringBuffer(new StringBuffer(str).reverse().toString().replaceFirst(ori, another)).reverse().toString();
	}

}
