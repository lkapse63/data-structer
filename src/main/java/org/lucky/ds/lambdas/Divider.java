package org.lucky.ds.lambdas;

public interface Divider {

    public double div(double nu, double deno);
}
