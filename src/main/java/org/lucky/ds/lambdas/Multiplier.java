package org.lucky.ds.lambdas;

public interface Multiplier {

    public double mul(double num);
}
