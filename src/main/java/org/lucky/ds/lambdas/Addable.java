package org.lucky.ds.lambdas;

interface  Addable {
    public double add(double a, double b);
}
