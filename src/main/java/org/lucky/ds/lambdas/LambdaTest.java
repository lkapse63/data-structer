package org.lucky.ds.lambdas;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

public class LambdaTest {

    public static  void main(String [] args){


        String[] lines = {"INFO CRIF SAOP REQUEST STARTED WATING FOR RESPONSE",
                "INFO] SOAP REQUEST MESSAGE",
                "DEBUG  [localhost-startStop-1] Running with Spring Boot v2.0.4.RELEASE, Spring v5.0.8.RELEASE",
                "ERROR org.springframework.context.support.AbstractApplicationContext  Exception encountered during context initialization",
                "WARN Framework Service 29-11-2018 03:04:27 PM ",
                "INFO Enforcing policies"
        };
        List<String> linelist = Arrays.asList(lines);

        Map<String, Integer> resultMap = linelist.stream()
                .flatMap(line -> Arrays.stream(line.split(" ")))
                .filter(world -> !world.isEmpty() && world.length() < 10 )
                .map(world -> world.toLowerCase(Locale.ROOT).replaceAll("[^a-zA-Z0-9]", ""))
                .map(word -> new AbstractMap.SimpleEntry<>(word, 1))
                .collect(toMap(e -> e.getKey(), e -> e.getValue(), (v1, v2) -> v1 + v2));

          resultMap.entrySet()
                  .stream()
                  .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                  .filter(entry -> entry.getValue()>=2)
                  .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new))
                  .forEach((k,v) -> System.out.println(k+" --> "+v));

    }
}
