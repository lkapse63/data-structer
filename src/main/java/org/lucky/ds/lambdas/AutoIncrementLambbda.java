package org.lucky.ds.lambdas;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public class AutoIncrementLambbda {

    public static void main(String[] args) throws Exception{
        AtomicInteger at = new AtomicInteger(0);

        /*for(int i=0;i<3;i++)
            at.incrementAndGet();
//        System.out.println( at.getAndIncrement());
        System.out.println( at.get());*/

        /*System.out.println("Working Directory = " +
                System.getProperty("user.dir")+
                "\n"+ Paths.get("").toAbsolutePath());*/
        ArrayList<String> name1=new ArrayList<String>();
        ArrayList<String> name2=new ArrayList<String>();
        String file="C:\\Users\\FWIN01977\\Desktop\\CRIF_UTILITY/crif-test-5.csv";
        BufferedReader reder= new BufferedReader(new FileReader(file));
        reder.lines()
                .forEach(
                        line ->{
                            String[] split = line.split(",");
                            name1.add(split[0].trim());
                            if(split.length==2)
                              name2.add(split[1].trim());
                        }
                );

        System.out.println("Size name1 "+name1.size());
        System.out.println("Size name2 "+name2.size());

//        System.out.println("name 2 => \n"+name2.toString());

        int notmatchCount=0;
        for(String name : name1){
            if(!name2.contains(name)){
                System.out.println(name);
                notmatchCount++;
            }

        }
        System.out.println("unique names: => "+notmatchCount);
    }
}
