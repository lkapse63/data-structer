package org.lucky.ds;



import org.apache.log4j.Logger;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OmniFileCopy {
    public static Logger logger = Logger.getLogger(OmniFileCopy.class);
    public static void main(String[] args) throws Exception{

        String basePath=args[0];
        String destPath=args[1];
        String date =args[2];
        /*String basePath="C:/EAppUpload/WFI_XML/";
        String destPath="C:/EAppUpload/omnies/";
        String date ="13-04-2019 04:28";*/

        SimpleDateFormat dateFormat= new SimpleDateFormat("dd-MM-yyyy hh:mm");
        Date date1 = dateFormat.parse(date);

        File[] directories = new File(basePath).listFiles(File::isDirectory);

        for(File file : directories){
            if(file.isDirectory()){
                File[] files= file.listFiles(File::isFile);
                for(File f : files){
                    if(! (f.lastModified() > date1.getTime()))
                        continue;
                    try{
                        Files.copy(f.toPath(), Paths.get(destPath+f.getName()), StandardCopyOption.REPLACE_EXISTING);
                        logger.info("files copy: "+f.getAbsolutePath());
                        break;
                    }catch (Exception e){
                        logger.error("files not copy: "+f.getAbsolutePath());
                    }

                }
          }
        }
    }
}
