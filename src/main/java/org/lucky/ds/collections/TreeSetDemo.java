package org.lucky.ds.collections;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetDemo {

    public static void main(String[] args) {
        Set<Integer> treeSet = new TreeSet<>();
        treeSet.add(10);
        treeSet.add(8);
        treeSet.add(5);
        treeSet.add(11);
        treeSet.add(13);
        treeSet.add(12);
        treeSet.add(9);
        treeSet.add(7);

        System.out.println(treeSet);

        Student st1 = new Student(18,"abc",'A',"12th");
        Student st2 = new Student(18,"abc",'A',"12th");
        Student st3 = new Student(19,"xyz",'A',"12th");

        Set<Student> treeStudent = new TreeSet<>();
        treeStudent.add(st1);
        treeStudent.add(st2);
        treeStudent.add(st3);
        System.out.println("Hascode st1 -> "+st1.hashCode());
        System.out.println("Hascode st2 -> "+st2.hashCode());
        System.out.println("Hascode st3 -> "+st3.hashCode());
        System.out.println(treeStudent);

    }
}
