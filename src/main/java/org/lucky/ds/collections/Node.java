package org.lucky.ds.collections;

import lombok.ToString;
import java.util.Iterator;
import java.util.Objects;


@ToString
public class Node<E> implements Iterable<Node<E>> {
    private E value;
    private Node<E> next;

    public Node(){
    }

    private Node(E value, Node<E> next) {
        this.value = value;
        this.next = next;
    }

    public void addNode(E value){
        if(this.value==null){
            this.value=value;
            return;
        }
        Node<E> that = this;
        while (that.next!=null){
            that=that.next;
        }
       that.next = new Node<>(value,null);
    }


    public boolean contains(E value){
        Node<E> that=this;
        while (that!=null){
            if(that.value.equals(value))
                return true;
            that=that.next;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node<?> node = (Node<?>) o;
        return Objects.equals(value, node.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public Iterator<Node<E>> iterator() {
        return new itr();
    }


    public E getValue() {
        return value;
    }

    private class itr implements Iterator<Node<E>>{
        Node<E> current=Node.this;
        @Override
        public boolean hasNext() {
            return Objects.nonNull(current);
        }

        @Override
        public Node<E> next() {
            Node<E> tmp = new Node<E>(current.value, current.next);
            current = current.next;
            return tmp;
        }
    }
}
