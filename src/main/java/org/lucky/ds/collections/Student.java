package org.lucky.ds.collections;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Student implements Comparable<Student>{
    private int age;
    private String name;
    private char div;
    private String st_class;

    @Override
    public int compareTo(Student o) {
        return Integer.valueOf(this.hashCode()).compareTo(Integer.valueOf(o.hashCode()));
    }
}
