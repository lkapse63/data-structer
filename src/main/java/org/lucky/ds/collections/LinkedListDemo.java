package org.lucky.ds.collections;

public class LinkedListDemo {
    public static void main(String[] args) {
        Node<Integer> node = new Node<>();

        for(int i=1;i<=100_000;i++)
            node.addNode(i);

        System.out.println("Contains 5 >> "+node.contains(5));

        for (Node<Integer> integerNode : node) {
            System.out.print(integerNode.getValue()+", ");
        }
        System.out.println();

    }
}
