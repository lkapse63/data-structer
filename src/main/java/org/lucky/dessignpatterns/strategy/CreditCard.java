package org.lucky.dessignpatterns.strategy;

public class CreditCard implements PaymentStrategy{

    private String cardNumber;
    private String expiry;
    private String cvv;
    private String name;

    public CreditCard(String cardNumber, String expiry, String cvv, String name) {
        this.cardNumber = cardNumber;
        this.expiry = expiry;
        this.cvv = cvv;
        this.name = name;
    }


    @Override
    public void pay(double amount) {
        System.out.println("Amount paid with credit/debit card "+amount);
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
