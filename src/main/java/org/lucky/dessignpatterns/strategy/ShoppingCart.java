package org.lucky.dessignpatterns.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ShoppingCart {

    private List<Item> itemList;

    public ShoppingCart(){
        itemList = new ArrayList<>();
    }

    public void addItem(Item item){
        this.itemList.add(item);
    }

    public double calculateTotal(){
        AtomicReference<Double> sum= new AtomicReference<>(0.0);
        itemList.forEach(item -> sum.updateAndGet(v -> new Double((double) (v + item.getPrice()))));
        return sum.get();
    }

    public void addAll(Item... items){
        for(Item item : items)
            this.addItem(item);
    }

    public void pay(PaymentStrategy paymentStrategy){
        paymentStrategy.pay(calculateTotal());
    }


}
