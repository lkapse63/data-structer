package org.lucky.dessignpatterns.strategy;

public class UPIPayment implements PaymentStrategy{
    private String mobileNumber;
    private String uipId;

    public UPIPayment(String mobileNumber, String uipId) {
        this.mobileNumber = mobileNumber;
        this.uipId = uipId;
    }

    @Override
    public void pay(double amount) {
        System.out.println("Amount paid with UPIPayment "+amount);
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUipId() {
        return uipId;
    }

    public void setUipId(String uipId) {
        this.uipId = uipId;
    }
}
