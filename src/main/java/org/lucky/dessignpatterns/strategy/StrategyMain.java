package org.lucky.dessignpatterns.strategy;

public class StrategyMain {

    public static void main(String[] args) {
        Item item1 = new Item("102",123.0);
        Item item2 = new Item("103",234.0);
        Item item3 = new Item("104",85.0);
        Item item4 = new Item("105",99.0);

        ShoppingCart cart = new ShoppingCart();
        cart.addAll(item1,item2,item3,item4);

        cart.pay(new UPIPayment("1234567890","uip@uip"));
        cart.pay(new CreditCard("123456789","23/34","123","foo"));
    }
}
