package org.lucky.dessignpatterns.strategy;

public interface PaymentStrategy {

    void pay(double amount);
}
