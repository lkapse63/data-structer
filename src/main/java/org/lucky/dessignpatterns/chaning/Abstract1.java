package org.lucky.dessignpatterns.chaning;

public class Abstract1 implements Base{

    protected Base base;

    @Override
    public void method1() {
      base.method1();
    }

    public Abstract1(Base base) {
        this.base = base;
    }
}
