package org.lucky.dessignpatterns.chaning;

public abstract class Abstract2 extends Abstract1 {

    public Abstract2(Base base) {
        super(base);
    }

    @Override
    public void method1() {
        System.out.println("New version caller");
        super.method1();
    }
}
