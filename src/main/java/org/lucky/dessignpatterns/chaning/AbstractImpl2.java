package org.lucky.dessignpatterns.chaning;

public class AbstractImpl2 extends Abstract1{

    public AbstractImpl2(Base base) {
        super(base);
    }

    @Override
    public void method1() {
        System.out.println("from AbstractImpl2");
        super.method1();
    }
}