package org.lucky.dessignpatterns.chaning;

public class ChaningTest {

    public static void main(String[] args) {
        BaseImpl base = new BaseImpl();
        AbstractImpl1 impl1 = new AbstractImpl1(base);

        AbstractImpl2 impl2=new AbstractImpl2(impl1);

        Abstract2Impl1 impl21 = new Abstract2Impl1(impl2);
        impl21.method1();

        System.out.println("----------------------------------");
        impl2.method1();
    }
}
