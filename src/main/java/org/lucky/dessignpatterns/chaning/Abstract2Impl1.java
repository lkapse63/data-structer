package org.lucky.dessignpatterns.chaning;

public class Abstract2Impl1 extends Abstract2{
    public Abstract2Impl1(Base base) {
        super(base);
    }

    @Override
    public void method1() {
        System.out.println("from Abstract2Impl1");
        super.method1();
    }
}
