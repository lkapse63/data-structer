package org.lucky.dessignpatterns.chaning;

public class AbstractImpl1 extends Abstract1{

    public AbstractImpl1(Base base) {
        super(base);
    }

    @Override
    public void method1() {
        System.out.println("from AbstractImpl1");
        super.method1();
    }
}
