package org.lucky.dessignpatterns.behaviroal.observerpattern;

public class NewsChannel implements ObserverListner {
    private String news;

    @Override
    public void update(Object o) {
        this.setNews((String)o);
    }

    public String getNews() {
        return news;
    }

    private void setNews(String news) {
        this.news = news;
    }

    @Override
    public String toString() {
        return "NewsChannel{" +
                "news='" + news + '\'' +
                '}';
    }
}
