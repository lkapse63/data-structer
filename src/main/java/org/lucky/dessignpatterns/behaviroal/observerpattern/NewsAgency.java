package org.lucky.dessignpatterns.behaviroal.observerpattern;


public class NewsAgency extends Observer {

    @Override
    public void setData(Object news) {
        notifyObserver(news);
    }
}
