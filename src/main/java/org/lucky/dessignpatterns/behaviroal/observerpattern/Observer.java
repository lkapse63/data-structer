package org.lucky.dessignpatterns.behaviroal.observerpattern;

import java.util.ArrayList;
import java.util.List;

public abstract class Observer {
    private final List<ObserverListner> observarbles = new ArrayList<>();

    public void addObserver(ObserverListner channel){
        observarbles.add(channel);
    }

    public void removeChannel(ObserverListner channel){
        observarbles.remove(channel);
    }

    public abstract void setData(Object o);

    protected void notifyObserver(Object o) {
        for(ObserverListner ch : observarbles)
            ch.update(o);
    }
}
