package org.lucky.dessignpatterns.behaviroal.observerpattern;

public interface ObserverListner {
    void update(Object o);
}
