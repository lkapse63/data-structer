package org.lucky.dessignpatterns.behaviroal.observerpattern;

public class NewsApp {
    public static void main(String[] args) {
        NewsAgency agency = new NewsAgency();
        NewsChannel ch1 = new NewsChannel();
        NewsChannel ch2 = new NewsChannel();

        agency.addObserver(ch1);
        agency.addObserver(ch2);

        agency.setData("news1");
        System.out.println("ch1-> "+ch1);
        System.out.println("ch2-> "+ch2);

        agency.setData("news 2");
        System.out.println("ch1-> "+ch1);
        System.out.println("ch2-> "+ch2);

        agency.removeChannel(ch2);

        agency.setData("news 3");
        System.out.println("ch1-> "+ch1);
        System.out.println("ch2-> "+ch2);

    }
}
