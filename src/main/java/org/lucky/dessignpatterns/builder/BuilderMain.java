package org.lucky.dessignpatterns.builder;

public class BuilderMain {

    public static void main(String[] args) {
        Computer computer = new Computer.ComputerBuilder()
                .HDD("500")
                .OS("linux")
                .RAM("16")
                .build();
        System.out.println(computer);

        Computer computer2 = new Computer.ComputerBuilder()
                .HDD("1TB")
                .OS("Windows")
                .RAM("32")
                .isBluetoothEnable(true)
                .isGraphicCardEnable(true)
                .build();
        System.out.println(computer2);
    }
}
