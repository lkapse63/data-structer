package org.lucky.dessignpatterns.builder;

public class Computer {

    private String HDD;
    private String RAM;
    private String OS;

    private boolean isGraphicCardEnable;
    private boolean isBluetoothEnable;

    private Computer(ComputerBuilder builder) {
        this.HDD = builder.HDD;
        this.RAM = builder.RAM;
        this.OS = builder.OS;
        this.isGraphicCardEnable = builder.isGraphicCardEnable;
        this.isBluetoothEnable = builder.isBluetoothEnable;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "HDD='" + HDD + '\'' +
                ", RAM='" + RAM + '\'' +
                ", OS='" + OS + '\'' +
                ", isGraphicCardEnable=" + isGraphicCardEnable +
                ", isBluetoothEnable=" + isBluetoothEnable +
                '}';
    }

    public static class ComputerBuilder{

        private String HDD;
        private String RAM;
        private String OS;

        private boolean isGraphicCardEnable;
        private boolean isBluetoothEnable;

        public ComputerBuilder HDD(String HDD){
            this.HDD=HDD;
            return this;
        }

        public ComputerBuilder RAM(String RAM){
            this.RAM=RAM;
            return this;
        }

        public ComputerBuilder OS(String OS){
            this.OS=OS;
            return this;
        }

        public ComputerBuilder isGraphicCardEnable(boolean isGraphicCardEnable){
            this.isGraphicCardEnable=isGraphicCardEnable;
            return this;
        }

        public ComputerBuilder isBluetoothEnable(boolean isBluetoothEnable){
            this.isBluetoothEnable=isBluetoothEnable;
            return this;
        }

        public Computer build(){
            return new Computer(this);
        }

    }
}
