package org.lucky.dessignpatterns.creational.abstractpatterns;


class PmWindow {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PmWindow{" +
                "name='" + name + '\'' +
                '}';
    }
}
