package org.lucky.dessignpatterns.creational.abstractpatterns;

public interface Window {
    Window createWindow();
}
