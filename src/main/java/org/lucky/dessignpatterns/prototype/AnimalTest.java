package org.lucky.dessignpatterns.prototype;

import org.checkerframework.checker.units.qual.A;

public class AnimalTest {

    public static void main(String[] args) throws Exception {
        Animal animal1=new Animal(4,2);
        System.out.println(System.identityHashCode(animal1)+" >> "+animal1);
        Animal animal2 = (Animal)animal1.clone();
        System.out.println(System.identityHashCode(animal2)+" >> "+animal2);
    }
}
