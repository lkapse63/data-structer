package org.lucky.dessignpatterns.prototype;

public class Animal implements Cloneable{

    private int legs;
    private int eyes;

    public Animal(int legs, int eyes) {
        this.legs = legs;
        this.eyes = eyes;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "Animal{" +
                "legs=" + legs +
                ", eyes=" + eyes +
                '}';
    }
}
