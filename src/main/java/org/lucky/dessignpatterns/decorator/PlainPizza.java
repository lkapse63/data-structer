package org.lucky.dessignpatterns.decorator;

public class PlainPizza implements Pizza{
    @Override
    public String getDescriptions() {
        return "Plain Dough";
    }

    @Override
    public double price() {
        return 20;
    }

    @Override
    public String toString() {
        return getDescriptions()+" "+price();
    }
}
