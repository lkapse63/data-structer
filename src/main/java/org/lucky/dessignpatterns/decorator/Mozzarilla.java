package org.lucky.dessignpatterns.decorator;

public class Mozzarilla extends PizzaDecorator{

    public Mozzarilla(Pizza pizza) {
        super(pizza);
    }

    @Override
    public String getDescriptions() {
        return super.getDescriptions()+" mozzarila";
    }

    @Override
    public double price() {
        double price = super.price()+35;
        return price;
    }

    @Override
    public String toString() {
        return getDescriptions()+" "+price();
    }

}
