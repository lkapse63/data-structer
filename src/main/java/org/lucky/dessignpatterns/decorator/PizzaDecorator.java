package org.lucky.dessignpatterns.decorator;

public abstract class PizzaDecorator implements Pizza {

    protected Pizza pizza;

    public PizzaDecorator(Pizza pizza){
        this.pizza=pizza;
    }

    @Override
    public String getDescriptions() {
        return pizza.getDescriptions();
    }

    @Override
    public double price() {
        return pizza.price();
    }

}
