package org.lucky.dessignpatterns.decorator;

public class TomatoSouce extends PizzaDecorator{

    public TomatoSouce(Pizza pizza) {
        super(pizza);
    }

    @Override
    public String getDescriptions() {
        return super.getDescriptions()+" Tomato souce";
    }

    @Override
    public double price() {
        double price = super.price()+20;
        return price;
    }
    @Override
    public String toString() {
        return getDescriptions()+" "+price();
    }

}
