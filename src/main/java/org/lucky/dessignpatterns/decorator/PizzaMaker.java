package org.lucky.dessignpatterns.decorator;

public class PizzaMaker {

    public static void main(String[] args) {
        PlainPizza plainPizza = new PlainPizza();
        System.out.println(plainPizza);

        TomatoSouce tomatoSouce = new TomatoSouce(plainPizza);
        System.out.println(tomatoSouce);

        System.out.println(new Mozzarilla(plainPizza));
        System.out.println(new Mozzarilla(tomatoSouce));


    }
}
