package org.lucky.dessignpatterns.decorator;

public interface Pizza {

    String getDescriptions();

    double price();
}
