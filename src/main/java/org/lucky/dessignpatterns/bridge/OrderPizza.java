package org.lucky.dessignpatterns.bridge;

public class OrderPizza {

    public static void main(String[] args) {
        new DominoseRestorent(new VeggiePizza()).orderPizza();
        new MacDRestrarent(new VeggiePizza()).orderPizza();
        new DominoseRestorent(new ChickenPizza()).orderPizza();
    }
}
