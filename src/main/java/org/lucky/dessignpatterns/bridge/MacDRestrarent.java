package org.lucky.dessignpatterns.bridge;

public class MacDRestrarent extends Restarent{

    public MacDRestrarent(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void deliver() {
        System.out.println("Adding MacD flavor");
        System.out.println("Pizza deliver....");
    }
}
