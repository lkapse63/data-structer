package org.lucky.dessignpatterns.bridge;

public abstract class Restarent{
    protected Pizza pizza;

    public Restarent(Pizza pizza){
        this.pizza=pizza;
    }

    public void orderPizza(){
        pizza.assemble();
        deliver();
    }

    public abstract void deliver();
}
