package org.lucky.dessignpatterns.bridge;

public class ChickenPizza implements Pizza{
    @Override
    public void assemble() {
        System.out.println("adding chicken pices");
        System.out.println("adding chicken souce");
        System.out.println("chicken pizza done");
    }
}
