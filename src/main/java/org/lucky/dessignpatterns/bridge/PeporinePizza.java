package org.lucky.dessignpatterns.bridge;

public class PeporinePizza implements Pizza{
    @Override
    public void assemble() {
        System.out.println("adding peporine");
        System.out.println("adding souce");
        System.out.println("peporine pizza done");
    }
}
