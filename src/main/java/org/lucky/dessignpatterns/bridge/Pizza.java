package org.lucky.dessignpatterns.bridge;

public interface  Pizza {

    public void assemble();
}
