package org.lucky.dessignpatterns.bridge;

public class VeggiePizza implements Pizza{
    @Override
    public void assemble() {
        System.out.println("adding veggie");
        System.out.println("adding crust");
        System.out.println("veggie pizza done");
    }
}
