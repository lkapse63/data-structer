package org.lucky.dessignpatterns.bridge;

public class DominoseRestorent extends Restarent{

    public DominoseRestorent(Pizza pizza) {
        super(pizza);
    }

    @Override
    public void deliver() {
        System.out.println("Adding dominoze flavor");
        System.out.println("Pizza deliver....");
    }
}
