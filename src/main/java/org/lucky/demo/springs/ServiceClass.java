package org.lucky.demo.springs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ServiceClass {

    @Autowired
    private RestTemplate restTemplate;

    public void serviceMethod(){
        System.out.println("Service method....");
        System.out.println("restTemplate >> "+restTemplate);
    }
}
