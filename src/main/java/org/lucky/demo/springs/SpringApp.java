package org.lucky.demo.springs;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SpringApp {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context=new AnnotationConfigApplicationContext(Config.class);
        System.out.println(context);

        AbstractCalss abstractCalss = context.getBean(AbstractCalssImpl.class);
        abstractCalss.nonAbstarctMethod();
    }
}
