package org.lucky.demo.springs.email;

import org.apache.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.time.LocalDate;
import java.util.Properties;

public class SendMailDemo {

    private static final Logger logger = Logger.getLogger(SendMailDemo.class);

    public static void main(String[] args) throws Exception {
//          SendMailDemo.sendMail("lkapse63@gmail.com","testing","test");
        SendMailDemo.fulcrumMail();
    }

    public static void sendMail(String mail,String sub,String mess) throws AddressException, MessagingException {
        String className=SendMailDemo.class.getName();
        logger.info(className +"entering sendMail "+mail);
        Properties props = new Properties();
        props.put("mail.smtp.host", "10.90.141.145");
        props.put("mail.smtp.socketFactory.port", "25");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("lankesh.kapse@fulcrumdigital.com","flip@1612");
                    }
                });

        MimeMessage message = new MimeMessage(session);
        message.setFrom(new InternetAddress("lankesh.kapse@fulcrumdigital.com"));
        message.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(mail));
        message.setSubject(sub);
        message.setContent(mess,
                "text/html" );
        Transport.send(message);
        logger.info(className+"leaving sendMail");
    }


    public static void fulcrumMail() throws Exception{


        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();

        mailSender.setHost("10.90.141.145");
        mailSender.setPort(25);
        mailSender.setUsername("CPSupport@bharti-axalife.com");
        mailSender.setPassword("test");
        mailSender.setProtocol("smtp");

        Properties javaMailProperties =new Properties();
        javaMailProperties.put("mail.smtp.auth","true");
        javaMailProperties.put("mail.smtp.starttls.enable","false");
        javaMailProperties.put("mail.smtp.ssl.trust","false");
        javaMailProperties.put("mail.smtp.quitwait","true");

        mailSender.setJavaMailProperties(javaMailProperties);

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper;

        mimeMessageHelper = new MimeMessageHelper(mimeMessage, true);
        mimeMessageHelper.setFrom("CPSupport@bharti-axalife.com");

        mimeMessageHelper.setTo("lankesh.kapse@fulcrumdigital.com");

        mimeMessageHelper.setSubject("SI TEST "+ LocalDate.now());
        mimeMessageHelper.setText("Testing....", true);

        File file = new File("C:\\POS\\si_tmp\\SI_REPORT\\PAYMENT_REPORT_2019-07-16.xlsx");
        FileSystemResource fileSystemResource = new FileSystemResource(file);
        mimeMessageHelper.addAttachment(file.getName(), fileSystemResource);

        mailSender.send(mimeMessage);


        /**
         * HTML message body
         */

        StringBuilder myvar = new StringBuilder();
        myvar.append("<!DOCTYPE html>")
                .append("<html>")
                .append("<body>")
                .append("")
                .append("<style>")
                .append("  table {")
                .append("    border-collapse: collapse;")
                .append("  }")
                .append("  th, td {")
                .append("    border: 1px solid orange;")
                .append("    padding: 10px;")
                .append("    text-align: left;")
                .append("  }")
                .append("</style>")
                .append("")
                .append("<p><b>Si Analysis Report</b></p>")
                .append("")
                .append("<table>")
                .append("  <tr>")
                .append("    <th>Total SI Records</th>")
                .append("    <th>Total Processed</th> ")
                .append("    <th>Success Count</th>")
                .append("    <th>Fail Count</th>")
                .append("  </tr>")
                .append("  <tr>")
                .append("    <td>1000</td>")
                .append("    <td>800</td>")
                .append("    <td>600</td>")
                .append("    <td>200</td>")
                .append("  </tr>")
                .append("</table>")
                .append("</body>")
                .append("</html>");



    }

}


